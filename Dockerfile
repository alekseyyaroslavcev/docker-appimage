FROM debian:jessie
MAINTAINER Aleksey Yaroslavcev <a.yaroslavcev@gmail.com>

RUN apt-get update;apt-get -y install wget curl git libglib2.0-0 file lsb libusb-1.0-0-dev libssl-dev

RUN curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash;

RUN apt-get -y install git-lfs;git lfs install

RUN apt-get -y install qt5-default build-essential

RUN apt-get -y install python3 	python3-pip; pip3 install xmlrpclib

RUN cd /opt;wget https://github.com/AppImage/AppImageKit/releases/download/continuous/appimagetool-x86_64.AppImage;chmod +x appimagetool-x86_64.AppImage

RUN cd /opt;lsof;./appimagetool-x86_64.AppImage --appimage-extract
